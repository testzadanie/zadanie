<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ZakazInserController extends Controller
{
   public function insert(Request $request){

         $name_client = $request->input('name_client');
         $id_client= $request->input('id_client');
         $status = $request->input('status');
         $company = $request->input('company');
         $filial = $request->input('filial');
         $city = $request->input('city');
         $addres = $request->input('addres');
         $opisanie_problem = $request->input('opisanie_problem');

         DB::table('zakaz')->insert([['name_client'=>$name_client, 'id_client'=>$id_client, 'data'=>NOW(), 'name_company'=>$company, 'name_filial'=>$filial, 'city_name'=>$city,  'address'=>$addres, 'opisanie_problem'=>$opisanie_problem, 'status'=>$status]]);

         echo "Ваш заказ успешно офформлен в ближайшее время с Вами свяжется менеджер.<br/>";

         echo '<a href = "/home">Перйти в личный кабинет</a>.';

         }

         }
