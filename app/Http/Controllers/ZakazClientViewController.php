<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ZakazClientViewController extends Controller
{
    public function index(){
    	$zakazy= DB::select('select * from zakaz');
    	return view ('myzakaz', ['zakazy'=>$zakazy]);
    }
}
