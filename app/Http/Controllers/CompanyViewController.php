<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyViewController extends Controller
{
    public function index(){
    	$company= DB::select('select * from company');
    	$filials= DB::select('select * from filial_company');
    	return view ('home', ['filials'=>$filials], ['company'=>$company]);
    }
}
