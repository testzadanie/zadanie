<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZakazTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zakaz', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_client');
            $table->string('id_client');
            $table->string('data');
            $table->string('name_company');
            $table->string('name_filial');
            $table->string('city_name');
            $table->string('address');
            $table->string('opisanie_problem');
            $table->string('manager');
            $table->string('In Process');
            $table->string('Resolved');
            $table->string('Closed');
            $table->string('opisanie_dorabotki');
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zakaz');
    }
}
