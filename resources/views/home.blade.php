@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Личный кабинет</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Вы зашли в личный кабинет как ! {{ Auth::user()->name }}
                    <h2 class="h2-bottom-line">Оформить заказ</h2>
                    <form action = "/create" class="form-publish" method = "post">
                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                        <input type = "hidden" name = "name_client" value = "{{ Auth::user()->name }}">
                        <input type = "hidden" name = "id_client" value = "{{ Auth::user()->id }}">
                        <input type = "hidden" name = "status" value = "Open">
                        <div class="inp-group">
                            <select name="company">
                                <option value="0">Выберите компанию*</option>
                                @foreach ($company as $compan)
                                    <option value="{{$compan->name_company}}">{{$compan->name_company}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="inp-group">
                            <select name="filial">
                                <option value="0">Выберите филиал*</option>
                                @foreach ($filials as $filial)
                                    <option value="{{$filial->name_filial}}">{{$filial->name_filial}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="inp-group">
                            <select name="city">
                                <option value="0">Выберите город филиала*</option>
                                @foreach ($filials as $filial)
                                    <option value="{{$filial->city_filial}}">{{$filial->city_filial}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="inp-group">
                            <input id="s_addres" type="text" name="addres" value="" placeholder="Точный адрес *" class="input">
                        </div>
                        <div class="inp-group">
                            <textarea name="opisanie_problem" cols="40" rows="3" placeholder="Опишите проблему *"></textarea>
                        </div> 
                        <div class="inp-group">
                            <p>* Обязательные поля</p>
                        </div>              
                        <div class="btn-center">
                            <center><div><button name="submit" class="button">Отправить заказ</button></div></center>
                        </div>
                    </form>
                    <form action = "/myzakaz" method = "get">
                        <center><div><button name="submit" class="button">Мои заказы</button></div></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
