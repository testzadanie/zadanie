@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Личный кабинет</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Ваши заказы
					<table border = 1>
						<tr>
							<td>Клиент</td>
							<td>Дата</td>
							<td>Компания</td>
							<td>Филиал</td>
							<td>Город</td>
							<td>Описание проблемы</td>
							<td>Отвественный менеджер</td>
							<td>Статус заказа</td>
						</tr>
						@foreach ($zakazy as $zakaz)
						<tr>
							<td>{{ $zakaz->name_client }}</td>
							<td>{{ $zakaz->data}}</td>
							<td>{{ $zakaz->name_company }}</td>
							<td>{{ $zakaz->name_filial}}</td>
							<td>{{ $zakaz->city_name }}</td>
							<td>{{ $zakaz->opisanie_problem}}</td>
							<td>{{ $zakaz->manager }}</td>
							<td>{{ $zakaz->status}}</td>
						</tr>
						@endforeach
					</table>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection